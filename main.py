import json
import requests


def main():
    session = requests.Session()
    base_url = "https://httpbin.org"

    query_params = {
        "param1": "foo",
        "param2": "bar"
    }

    headers = {
        "User-Agent": "chrome",
        "Authorization": "Bearer: ewtrjpkrt454"
    }

    payload = {
        "id": 32323,
        "name": "Python"
    }

    with open("text_descr.txt") as text_file:
        files = {
            "text_file": text_file
        }

        try:
            response = session.post(url=base_url + "/post",
                                    params=query_params,
                                    headers=headers,
                                    data=payload,
                                    files=files,
                                    timeout=0.001)

            response_2 = session.get(url=base_url + "/get",
                                     params=query_params,
                                     headers=headers,
                                     timeout=0.00001)
        except requests.exceptions.ConnectTimeout:
            print("Ресурс недоступен")

    # print(response.status_code)
    # print(response.text)
    # print(type(response.json()))
    # print(type(json.dumps(json.loads(response.text))))


if __name__ == "__main__":
    main()
